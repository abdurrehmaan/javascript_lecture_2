// consoles
console.log('learn javascript');
// variables

// 1st method of defining variables
var variable_1;

// 2nd method of defining variable.
let name = 'abdur rehman';
console.log(name);

name = "Abdur rehman Khalid";
console.log(name);


const intersetrate = 0.3;
console.log(intersetrate);

// dynamic vs static language
// what is static languages
// static => define type for the varibale
// dynamic => type can change

// rules and naming the variables
// Rule 1.  cannot use reserved names (e.g if, let, else etc)
// Rule 2. variable should have a meaningful name
// Rule 3. Variable names cannot start with a number
// Rule 4. variable name should not contain spaces or (-)
// Rule 5. Variable names are case sensitive


// we will always name variable by camelCase


// primitive type
// what are the type of values we can assign a variables
// javascript has two types of values
// 1. Primitive/value types
// 2. Reference type


// Array 
let selectedColor = ['red','green', 'blue'];
console.log(selectedColor);

// insertion
selectedColor[3] = 'lightblue';
console.log(selectedColor);

let dynamicArray = [1, 'name', [1,2,3], {1:name}];
console.log(dynamicArray);

// functions
function greet() {
    console.log('hello world')
}

greet();


function greetName(name, lastName) {
    console.log(`'hello, ${name} ${lastName}`);
}
greetName('Abdur', 'Rehman');

